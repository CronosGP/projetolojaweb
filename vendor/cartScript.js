$(document).ready(function(){
    var jsonStr = sessionStorage.getItem('carrinho');
    var obj = JSON.parse(jsonStr);
    var tam = 0;
    var total = 0;
        obj['produtosList'].forEach(item=>{
            total = total+(item.preco*item.quantidade)
            tam = (tam+1);
            $('#cartList').append(
                `<tr>
                <td><img src="`+ item.img +`"/ style="width:15%;height:auto"> </td>
                <td>`+ item.nome +`</td>
                <td>In stock</td>
                <td><input class="form-control" type="text" value="`+ item.quantidade +`" /></td>
                <td class="text-right">`+item.preco+`</td>
                <td class="text-right"><button id="`+item.id+`" class="cartRemov btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>
                </tr>`
            );
        });

        $('#cartList').append(
        `<tr>                            
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><strong>Total</strong></td>
        <td class="text-right"><strong>`+total+`</strong></td>
        </tr>`
        );
        console.log(obj);
        console.log(tam);
        $('#cartNumber').append(
            `<i class="fa fa-shopping-cart"></i> Cart
            <span class="badge badge-light">`+ tam +`</span>`
            );
        
        $('.cartRemov').on('click', function(){
            let prodId = parseInt($(this).attr('id'));
            let arr = obj['produtosList'].filter(item => item.id != prodId);
            obj['produtosList'] = arr;
            console.log(obj['produtosList']);
            obj['produtosList'].forEach(item=>{
                $('#cartList').html(
                    `<tr>
                    <td><img src="`+ item.img +`"/ style="width:15%;height:auto"> </td>
                    <td>`+ item.nome +`</td>
                    <td>In stock</td>
                    <td><input class="form-control" type="text" value="`+item.quantidade+`" /></td>
                    <td class="text-right">`+item.preco+`</td>
                    <td class="text-right"><button id="`+item.id+`" class="cartRemov btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button> </td>
                    </tr>`
                );
            });
            sessionStorage.setItem('carrinho', JSON.stringify(obj));
            location.reload();
        });
    
    
});