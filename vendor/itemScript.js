$(document).ready(function(){
    let prod_id = parseInt(location.search.replace('?prod_id=', '')) || null;
    if(prod_id === null){
        location = 'index.html'
    }else{
        $.getJSON('vendor/dados.json', function(data){
             item = data.filter(i => i.id == prod_id)[0];
             document.title = item.nome
             $('#item').html(

                `<div class="card mt-4">
                <img class="card-img-center img-fluid mx-auto" src="`+item.img+`" alt="" style="width:50%;height:auto">
                <div class="card text-center">
                <div class="card-body">

                <h3 class="card-title">` + item.nome + `</h3>
                <h4>$`+item.preco+`</h4>
                <p class="card-text">`+item.desc+`</p>
                  
                </div>
                <a id="add" class="btn btn-success btn-lg btn-block" href="cart.html">COMPRAR</a>
                </div>` 
         
             )
 
            $('#add').on('click', function(){
                var jsonStr = sessionStorage.getItem('carrinho');
                var obj = JSON.parse(jsonStr);
                let arr = obj['produtosList'].filter(prod => prod.id == item.id);
                if(arr.length == 0){
                    item.quantidade = 1;
                    obj['produtosList'].push(item);
                 console.log(obj);
                }else{
                    let diferentes = obj['produtosList'].filter(prod => prod.id !== item.id);
                    obj['produtosList'] = diferentes;
                    obj['produtosList'].push(item);

                }
                
                console.log(obj);
                sessionStorage.setItem('carrinho', JSON.stringify(obj));
            });
        });
        var tam = 0;
        var jsonStr = sessionStorage.getItem('carrinho');
        var obj = JSON.parse(jsonStr);
        obj['produtosList'].forEach(item=>{
            tam = (tam+1);
        }),
        $('#cartNumber').append(
          `<i class="fa fa-shopping-cart"></i> Cart
          <span class="badge badge-light">`+ tam +`</span>`
          );
   } 
});